import router from "../router";

export default {
  addProduct({ commit, state }, payload) {
    commit("setLoading", true);
    commit("setError", null);
    commit("setSuccess", false);
    commit("increaseId");
    const product = {
      id: state.id,
      name: payload.name,
      unitPurchaseCost: payload.unitPurchaseCost,
      unitSellingCost: payload.unitSellingCost,
      productType: payload.productType,
      quantity: payload.quantity,
      unitsSold: payload.unitsSold,
      profitPercentage: payload.profitPercentage
    };
    try {
      commit("addProduct", product);
      commit("setLoading", false);
      commit("setSuccess", true);
    } catch (err) {
      commit("setLoading", false);
      commit("setError", err);
    }
  },
  deleteProduct({ commit }, payload) {
    commit("setLoading", true);
    commit("setError", null);
    commit("setSuccess", false);
    try {
      commit("deleteProduct", payload.id);
      commit("setLoading", false);
      commit("setSuccess", true);
    } catch (err) {
      commit("setLoading", false);
      commit("setError", err);
    }
  },
  updateProduct({ commit }, payload) {
    commit("setLoading", true);
    commit("setError", null);
    commit("setSuccess", false);
    try {
      commit("updateProduct", payload);
      commit("setLoading", false);
      commit("setSuccess", true);
    } catch (error) {
      commit("setLoading", false);
      commit("setError", error);
    }
  },
  signInUser({ state, commit }, payload) {
    commit("setLoading", true);
    commit("setError", null);

    try {
      if (
        state.user.email === payload.email &&
        state.user.password === payload.password
      ) {
        commit("setLoading", null);
        commit("isLoggedIn", true);
        // router.push('/')
      } else {
        commit("setLoading", null);
        commit("setError", true);
      }
    } catch (err) {
      commit("setLoading", null);
      commit("setError", err);
    }
  },

  logOut({ commit }) {
    commit("isLoggedIn", false);
    router.push("/signin");
  }
};
