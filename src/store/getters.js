export default {
  loadProduct(state) {
    return productId => {
      return state.products.find(product => {
        return product.id === productId;
      });
    };
  },
  profitableProducts(state) {
    const products = state.products.sort((productA, productB) => {
      return productB.profitPercentage - productA.profitPercentage;
    });
    console.log(products.slice(0, 5));
    return products.slice(0, 5);
  },
  totalProductsSoldPerProduct (state) {
    const chartData = [];
    state.products.forEach(element => {
      const array = [element.name, element.unitsSold];
      chartData.push(array)
    });
    return chartData;
  }
};
