export default {
  increaseId(state) {
    state.id = state.id + 1;
  },
  addProduct(state, payload) {
    state.products.push(payload);
  },
  deleteProduct(state, payload) {
    state.products.splice(
      state.products.findIndex(product => product.id === payload),
      1
    );
  },
  updateProduct(state, payload) {
    const product = state.products.find(product => {
      return product.id === payload.id;
    });
    product.name = payload.name;
    product.unitPurchaseCost = payload.unitPurchaseCost;
    product.unitSellingCost = payload.unitSellingCost;
    product.productType = payload.productType;
    product.quantity = payload.quantity;
    product.unitsSold = payload.unitsSold;
    product.profitPercentage = payload.profitPercentage;
  },
  isLoggedIn(state, payload) {
    state.isLoggedIn = payload;
  },
  setError(state, payload) {
    state.error = payload;
  },
  setSuccess(state, payload) {
    state.success = payload;
  },
  setLoading(state, payload) {
    state.loading = payload;
  }
};
