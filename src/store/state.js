import products from "@/data/products";
export default {
  id: 6,
  products,
  productTypes: ["RAM", "Graphics Card", "Motherboard"],
  user: {
    email: "admin@test.com",
    password: "123456"
  },
  isLoggedIn: false,
  error: null,
  loading: false,
  success: false
};
