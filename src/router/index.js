import Vue from "vue";
import VueRouter from "vue-router";
import Signin from "@/views/Signin.vue";
import Home from "@/views/Home.vue";
// import AddProduct from '../views/Products/AddProduct.vue'
// import About from '../views/About.vue'
import DashView from "@/views/Dash.vue";
import NotFoundView from "@/views/404.vue";
import AuthGuard from "./auth-guard";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: DashView,
    children: [
      {
        path: "home",
        name: "home",
        alias: "",
        component: Home,
        beforeEnter: AuthGuard
      },
      {
        path: "products/all-products",
        name: "allproducts",
        component: () => import("@/views/Products/AllProducts.vue"),
        beforeEnter: AuthGuard
      },
      {
        path: "products/add-product",
        name: "addproduct",
        component: () => import("@/views/Products/AddProduct.vue"),
        beforeEnter: AuthGuard
      },
      {
        path: "products/:id",
        name: "product",
        component: () => import("@/views/Products/Product.vue"),
        props: true,
        beforeEnter: AuthGuard
      }
    ]
  },
  {
    path: "/signin",
    name: "Signin",
    component: Signin
  },
  {
    path: "/about",
    name: "About",
    component: () => import("@/views/About.vue")
  },
  {
    path: "*",
    component: NotFoundView
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
