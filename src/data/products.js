export default [
  {
    id: 1,
    name: "Adata 4 GB DDR4 2666 BUS Desktop",
    unitPurchaseCost: "34",
    unitSellingCost: "65",
    productType: "RAM",
    quantity: "6",
    unitsSold: "3",
    profitPercentage: 35
  },
  {
    id: 2,
    name: "TEAM ELITE U-Dimm 4GB 2400MHz DDR4",
    unitPurchaseCost: "34",
    unitSellingCost: "65",
    productType: "RAM",
    quantity: "6",
    unitsSold: "2",
    profitPercentage: 45
  },
  {
    id: 3,
    name: "ASRock A320M-HDV R4.0 AMD Motherboard",
    unitPurchaseCost: "34",
    unitSellingCost: "65",
    productType: "Motherboard",
    quantity: "6",
    unitsSold: "9",
    profitPercentage: -25
  },
  {
    id: 4,
    name: "Asus TUF Gaming B550 Plus ATX AM4 Motherboard",
    unitPurchaseCost: "34",
    unitSellingCost: "65",
    productType: "Motherboard",
    quantity: "6",
    unitsSold: "5",
    profitPercentage: 10
  },
  {
    id: 5,
    name: "Gigabyte GT 710 2GB DDR5 Graphics Card",
    unitPurchaseCost: "34",
    unitSellingCost: "65",
    productType: "Graphics Card",
    quantity: "6",
    unitsSold: "10",
    profitPercentage: 15
  },
  {
    id: 6,
    name: "Asus ROG Strix RX 5500XT 8GB Gaming Graphics Card",
    unitPurchaseCost: "34",
    unitSellingCost: "65",
    productType: "Graphics Card",
    quantity: "6",
    unitsSold: "3",
    profitPercentage: 5
  }
];
